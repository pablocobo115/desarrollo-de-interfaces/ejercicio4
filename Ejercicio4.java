package ejercicio7;

import java.util.Arrays;

public class Ejercicio4 {

    public static void main(String[] args) {

        int array[] = new int[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = i+1;
        }
        
        for (int i = 0; i < array.length; i++) {
            if ((i+1) % 2 == 0) {
                array[i]=0;
            }
        }

        System.out.println(Arrays.toString(array));

    }

}

